package name.alp.shopping.campaignservice.exceptions;

public class CategoryNotFoundException extends RuntimeException {
    public CategoryNotFoundException(String categoryId) {
        super("Category Not Found id:" + categoryId);
    }
}
