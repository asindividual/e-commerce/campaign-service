package name.alp.shopping.campaignservice;

import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class CampaignRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignRestController.class);
    private final CampaignService campaignService;

    public CampaignRestController(CampaignService campaignService) {
        this.campaignService  = campaignService;
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates")
    @PostMapping
    public Mono<Campaign> save(@RequestBody Campaign campaign) {
        LOGGER.info("create: {}", campaign);
        return campaignService.save(campaign);
    }

    @Operation(summary = "Provides an entity for the specified id",
            description = "")
    @GetMapping("/{id}")
    public Mono<Campaign> findById(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return campaignService.findById(id);
    }

    @Operation(summary = "Provides all entity as a list",
            description = "")
    @GetMapping
    public Flux<Campaign> findAll() {
        LOGGER.info("findAll");
        return campaignService.findAll();
    }

    @Operation(summary = "Provides all entities by the Categori specified as a list",
            description = "")
    @GetMapping("/category/{category}")
    public Flux<Campaign> findByCategoryId(@PathVariable("category") String categoryId) {
        LOGGER.info("findByCategoryId: category={}", categoryId);
        return campaignService.findByCategoryId(categoryId);
    }

    @Operation(summary = "Provides all entities by the Categori specified as a list",
            description = "")
    @GetMapping("/category/{category}/totalamount/{totalamount}")
    public Flux<Campaign> findApplicableCampaignsByCategoryId(@PathVariable("category") String categoryId,@PathVariable("totalamount") Integer totalamount) {
        LOGGER.info("findByCategoryId: category={}", categoryId);
        return campaignService.findApplicableCampaignsByCategoryId(categoryId,totalamount);
    }

}
