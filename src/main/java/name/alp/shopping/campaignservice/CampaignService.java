package name.alp.shopping.campaignservice;

import name.alp.shopping.campaignservice.client.category.Category;
import name.alp.shopping.campaignservice.client.category.CategoryClient;
import name.alp.shopping.campaignservice.exceptions.CategoryNotFoundException;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CampaignService {

    private final CampaignRepository campaignRepository;
    private final CategoryClient categoryClient;

    public CampaignService(CampaignRepository campaignRepository, @Lazy CategoryClient categoryClient) {
        this.campaignRepository = campaignRepository;
        this.categoryClient = categoryClient;
    }

    public Mono<Campaign> save(Campaign campaign) {
        if(campaign.getCategoryIdList()!=null && !campaign.getCategoryIdList().isEmpty()){
            List<String> uptoDateCategoryList=new ArrayList<>();
            for (String category: campaign.getCategoryIdList()) {
                Category uptoDateCategory = categoryClient.findById(category);
                if(uptoDateCategory ==null){
                    throw new CategoryNotFoundException(category);
                }
                uptoDateCategoryList.add(uptoDateCategory.getId());
            }
            campaign.setCategoryIdList( uptoDateCategoryList.stream().distinct().collect(Collectors.toList()));
        }
        Mono<Campaign> result = campaignRepository.save(campaign);
        return result;
    }

    public Mono<Campaign> findById(String id) {
        return campaignRepository.findById(id);
    }


    public Flux<Campaign> findAll() {
        return campaignRepository.findAll();
    }

    public Flux<Campaign> findByCategoryId(String categoryId) {
        return campaignRepository.findByCategoryId(categoryId);
    }

    public Flux<Campaign> findApplicableCampaignsByCategoryId(String categoryId,Integer totalAmount) {
        return campaignRepository.findApplicableCampaignsByCategoryId(categoryId,totalAmount);
    }
}
