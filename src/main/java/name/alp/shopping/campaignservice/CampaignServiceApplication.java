package name.alp.shopping.campaignservice;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
@EnableDiscoveryClient
public class CampaignServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CampaignServiceApplication.class, args);
    }

    @Bean
    public OpenAPI customOpenAPI(@Value("${info.app.maven.description}") String appDesciption, @Value("${info.app.maven.version}") String appVersion) {
        return new OpenAPI()
                .info(new Info()
                        .title("Campaign API for Shopping application")
                        .version(appVersion)
                        .description(appDesciption)
                        .termsOfService("")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }
}
