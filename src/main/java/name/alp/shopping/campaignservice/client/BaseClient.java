package name.alp.shopping.campaignservice.client;

import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;

import java.lang.reflect.ParameterizedType;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class demonstrates a FeignClient interface implementation.
 * The FeignClient is not supported for reactive spring
 * Once the Reactive Feign client is supported by spring-cloud-starter-openfeign this is going to be replaced with spring equvalent interface
 * //@FeignClient(name = "category-service")
 */
public abstract class BaseClient<T> {


    protected final DiscoveryClient client;
    private final Class<T> type;
    private final Map<String, String> feignServices = new HashMap<>();
    private final String serviceKey;

    public BaseClient(DiscoveryClient client,String serviceKey,String serviceBaseUrl) {
        this.client = client;
        this.serviceKey=serviceKey;
        this.type = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

        feignServices.put(serviceKey, serviceBaseUrl);
    }

    protected String getBaseUrl(String serviceName) {
        return feignServices.get(serviceName);
    }

    protected <T> T block(Disposable subscribe, AtomicReference<T> reference) {
        while (!subscribe.isDisposed()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return reference.get();
    }

    protected WebClient getWebClient(String serviceName) {
        List<ServiceInstance> serviceInstances = client.getInstances(serviceName);
        if (serviceInstances.isEmpty()) {
            throw new RuntimeException("Couldn't find the dependent service");
        }

        DefaultServiceInstance serviceInstance = (DefaultServiceInstance) serviceInstances.iterator().next();
        String baseUrl = null;//Host()+":"+serviceInstance.getPort()+"";
        try {
            baseUrl = serviceInstance.getUri().toURL().toString();
        } catch (MalformedURLException e) {
            throw new RuntimeException("Couldn't connect to the dependent service");
        }
        return WebClient
                .builder()
                .baseUrl(baseUrl)
                .defaultCookie("cookieKey", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap("url", baseUrl))
                .build();
    }

    public <T> T findById(String id) {
        WebClient webClient = getWebClient(getService());
        AtomicReference<T> atomicCategory = new AtomicReference<>();

        String uri = getBaseUrl(getService()) + id;
        Mono<T> serviceCall = (Mono<T>) webClient.get()
                .uri(uri)
                .retrieve()
                .bodyToMono(this.type)
                .doOnSuccess(entity -> atomicCategory.set((T) entity));

        Disposable subscribe = serviceCall.subscribe();
        T result = block(subscribe, atomicCategory);

        return result;
    }

    public <T> T save(T entityToSave) {
        WebClient webClient = getWebClient(getService());
        AtomicReference<T> atomicCategory = new AtomicReference<>();

        String uri = getBaseUrl(getService());
        Mono<T> serviceCall = (Mono<T>) webClient.post()
                .uri(uri)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(entityToSave), this.type)
                .retrieve()
                .bodyToMono(this.type)
                .doOnSuccess(entity -> atomicCategory.set((T) entity));

        Disposable subscribe = serviceCall.subscribe();

        T result = block(subscribe, atomicCategory);

        return result;
    }

    protected String getService(){
        return serviceKey;
    }

}
