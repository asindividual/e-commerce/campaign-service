package name.alp.shopping.campaignservice;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;

@Repository
public interface CampaignRepository extends ReactiveCrudRepository<Campaign, String> {
    @Query("{ 'categoryIdList': ?#{[0]}  }")
    Flux<Campaign> findByCategoryId(String categoryId);

    @Query("{ 'categoryIdList': ?#{[0]} , 'rule':{ $lte: ?#{[1]} } }")
    Flux<Campaign> findApplicableCampaignsByCategoryId(String categoryId, Integer totalAmount);
}
