package name.alp.shopping.campaignservice;


import name.alp.shopping.campaignservice.Campaign;
import name.alp.shopping.campaignservice.CampaignRepository;
import name.alp.shopping.campaignservice.CampaignRestController;
import name.alp.shopping.campaignservice.CampaignService;
import name.alp.shopping.campaignservice.client.category.Category;
import name.alp.shopping.campaignservice.client.category.CategoryClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CampaignRestController.class)
@Import(CampaignService.class)
public class CampaignServiceTest {

    @Autowired
    private CampaignService campaignService;

    @MockBean
    CampaignRepository repository;
    @MockBean
    CategoryClient categoryClient;

    @Test
    public void saveTest(){
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign = new Campaign();
        campaign.setId("test_id1");
        campaign.setTitle("Test title");
        campaign.setRule(10);
        campaign.setDiscount(20D);
        campaign.setDiscountType(DiscountType.Rate);
        campaign.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));
        Mono<Campaign> expected = Mono.just(campaign);
        Mockito
                .when(repository.save(campaign))
                .thenReturn( expected);


        Mono<Campaign> actual =  campaignService.save(campaign);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void findById(){
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign = new Campaign();
        campaign.setId("test_id1");
        campaign.setTitle("Test title");
        campaign.setRule(10);
        campaign.setDiscount(20D);
        campaign.setDiscountType(DiscountType.Rate);
        campaign.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Mono<Campaign> expected = Mono.just(campaign);

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(expected);

        Mono<Campaign> actual =  campaignService.findById("test_id1");
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void findAllTest(){
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign1 = new Campaign();
        campaign1.setId("test_id1");
        campaign1.setTitle("Test title");
        campaign1.setRule(10);
        campaign1.setDiscount(20D);
        campaign1.setDiscountType(DiscountType.Rate);
        campaign1.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Campaign campaign2 = new Campaign();
        campaign2.setId("test_id2");
        campaign2.setTitle("Test title");
        campaign2.setRule(10);
        campaign2.setDiscount(20D);
        campaign2.setDiscountType(DiscountType.Rate);
        campaign2.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Mockito
                .when(repository.findAll())
                .thenReturn(Flux.fromArray(new Campaign[]{campaign1, campaign2}));

        Flux<Campaign> result =  campaignService.findAll();
        assertThat(result.collectList().block().size()).isEqualTo(2);
    }

    @Test
    public void findByCategoryId(){
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign1 = new Campaign();
        campaign1.setId("test_id1");
        campaign1.setTitle("Test title");
        campaign1.setRule(10);
        campaign1.setDiscount(20D);
        campaign1.setDiscountType(DiscountType.Rate);
        campaign1.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Campaign campaign2 = new Campaign();
        campaign2.setId("test_id2");
        campaign2.setTitle("Test title");
        campaign2.setRule(10);
        campaign2.setDiscount(20D);
        campaign2.setDiscountType(DiscountType.Rate);
        campaign2.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Mockito
                .when(repository.findByCategoryId(category.getId()))
                .thenReturn(Flux.fromArray(new Campaign[]{campaign1, campaign2}));

        Flux<Campaign> result =  campaignService.findByCategoryId(category.getId());
        assertThat(result.collectList().block().size()).isEqualTo(2);
    }
}
