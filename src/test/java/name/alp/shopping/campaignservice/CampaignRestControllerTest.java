package name.alp.shopping.campaignservice;


import name.alp.shopping.campaignservice.client.category.Category;
import name.alp.shopping.campaignservice.client.category.CategoryClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.util.Arrays;

import static org.mockito.Mockito.times;


@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CampaignRestController.class)
@Import(CampaignService.class)
public class CampaignRestControllerTest {

    @MockBean
    CampaignRepository repository;
    @MockBean
    CategoryClient categoryClient;
    @Autowired
    private WebTestClient webClient;

    @Test
    void testSave() {
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign = new Campaign();
        campaign.setId("test_id1");
        campaign.setTitle("Test title");
        campaign.setRule(10);
        campaign.setDiscount(20D);
        campaign.setDiscountType(DiscountType.Rate);
        campaign.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Mockito.when(repository.save(campaign)).thenReturn(Mono.just(campaign));

        webClient.post()
                .uri("/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(campaign))
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    void testFindById()
    {
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign = new Campaign();
        campaign.setId("test_id1");
        campaign.setTitle("Test title");
        campaign.setRule(10);
        campaign.setDiscount(20D);
        campaign.setDiscountType(DiscountType.Rate);
        campaign.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(campaign));

        webClient.get().uri("/{id}", "test_id1")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.title").isNotEmpty()
                .jsonPath("$.id").isEqualTo("test_id1");

        Mockito.verify(repository, times(1)).findById("test_id1");
    }

    @Test
    void testFindAll()
    {
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign1 = new Campaign();
        campaign1.setId("test_id1");
        campaign1.setTitle("Test title");
        campaign1.setRule(10);
        campaign1.setDiscount(20D);
        campaign1.setDiscountType(DiscountType.Rate);
        campaign1.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Campaign campaign2 = new Campaign();
        campaign2.setId("test_id2");
        campaign2.setTitle("Test title");
        campaign2.setRule(10);
        campaign2.setDiscount(20D);
        campaign2.setDiscountType(DiscountType.Rate);
        campaign2.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Campaign campaign3 = new Campaign();
        campaign3.setId("test_id2");
        campaign3.setTitle("Test title");
        campaign3.setRule(10);
        campaign3.setDiscount(20D);
        campaign3.setDiscountType(DiscountType.Rate);
        campaign3.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Mockito
                .when(repository.findAll())
                .thenReturn(Flux.fromArray(new Campaign[]{campaign1, campaign2, campaign3}));

        webClient.get().uri("/")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Campaign.class);


    }

    @Test
    public void findByCategoryId(){
        Category category=new Category( "test_category_id1","test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Campaign campaign1 = new Campaign();
        campaign1.setId("test_id1");
        campaign1.setTitle("Test title");
        campaign1.setRule(10);
        campaign1.setDiscount(20D);
        campaign1.setDiscountType(DiscountType.Rate);
        campaign1.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Campaign campaign2 = new Campaign();
        campaign2.setId("test_id2");
        campaign2.setTitle("Test title");
        campaign2.setRule(10);
        campaign2.setDiscount(20D);
        campaign2.setDiscountType(DiscountType.Rate);
        campaign2.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Campaign campaign3 = new Campaign();
        campaign3.setId("test_id2");
        campaign3.setTitle("Test title");
        campaign3.setRule(10);
        campaign3.setDiscount(20D);
        campaign3.setDiscountType(DiscountType.Rate);
        campaign3.setCategoryIdList(Arrays.asList(new String[]{category.getId()}));

        Mockito
                .when(repository.findByCategoryId(category.getId()))
                .thenReturn(Flux.fromArray(new Campaign[]{campaign1, campaign2}));

        webClient.get().uri("/category/",category.getId())
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Campaign.class);
    }
}
